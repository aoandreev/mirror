﻿namespace SGlass.Speech
{
    public interface ICmdInterpreter
    {
        CmdContext GetPhrsIntent(string phrase);
    }
}