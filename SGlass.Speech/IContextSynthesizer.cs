﻿using System;
using System.Threading.Tasks;

namespace SGlass.Speech
{
    public interface ICntxtSynthesizer
    {
        Task<string> GetCntxtMessageAsync(DateTime? dateContext = null);
    }
}