﻿using System;

namespace SGlass.Speech
{
    public struct CmdContext
    {
        public Command Command { get; private set; }

        public DateTime? DateContext { get; private set; }

        public CmdContext(Command command, DateTime? dateContext = null)
        {
            Command = command;
            DateContext = dateContext;
        }

        public static implicit operator Command(CmdContext context) => context.Command;

        public static implicit operator CmdContext(Command command) => new CmdContext(command);
    }
}