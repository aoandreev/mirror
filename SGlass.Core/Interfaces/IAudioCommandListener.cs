﻿using System.Threading.Tasks;

namespace SGlass.Interfaces
{
    public interface IAudioCommandListener
    {
        Task PlayRandomSongAsync();
    }
}