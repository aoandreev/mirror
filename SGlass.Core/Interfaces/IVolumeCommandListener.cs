﻿using System.Threading.Tasks;

namespace SGlass.Interfaces
{
    public interface IVolumeCommandListener
    {
        Task SetVolumeAsync(string phrase);
    }
}