﻿using System.Threading.Tasks;

namespace SGlass.Threading
{
    public delegate Task AsyncEventHandler<TEventArgs>(object sender, TEventArgs e);
}