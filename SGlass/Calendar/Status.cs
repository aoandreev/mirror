﻿

namespace SGlass.Calendar
{
    public enum Status
    {
        Tentative,
        Confirmed,
        Cancelled
    };
}