﻿using System;
using System.Threading.Tasks;
using SGlass.Extensions;
using SGlass.ViewModels;
using Windows.UI.Xaml;

namespace SGlass.Controls
{
    public static class SpeechControlHelper
    {
        public static Task<string> GetCntxtMessageAsync(
            DependencyObject dependency, 
            object dataContext, 
            DateTime? dateContext = null,
            string unableToGenerateSpeechMessage = "Sorry, I'm unable to process your request.")
            => dependency.ThreadSafeAsync(
                () => dataContext is BaseViewModel viewModel 
                    ? viewModel.ToFormattedString(dateContext) 
                    : unableToGenerateSpeechMessage);
    }
}