﻿using System.Threading.Tasks;

namespace SGlass.Controls
{
    interface IAsyncLoader
    {
        Task LoadAsync();
    }
}